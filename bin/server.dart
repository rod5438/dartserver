// Copyright (c) 2014, the Dart project authors.  Please see the AUTHORS file
// for details. All rights reserved. Use of this source code is governed by a
// BSD-style license that can be found in the LICENSE file.

// Use the client program, number_guesser.dart to automatically make guesses.
// Or, you can manually guess the number using the URL localhost:4045/?q=#,
// where # is your guess.
// Or, you can use the make_a_guess.html UI.

import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:path/path.dart' as p;
import 'dart:math' show Random;

Random intGenerator = Random();
String certificateChain = 'server_chain.pem';
String serverKey = 'server_key.pem';

Future main() async {
  print('Server launching');
  Directory.current = p.dirname(Platform.script.toFilePath());
  var serverContext = SecurityContext(); /*1*/
  serverContext.useCertificateChain(certificateChain); /*2*/
  serverContext.usePrivateKey(serverKey, password: 'dartdart'); /*3*/

  final server = await HttpServer.bind(
    InternetAddress.loopbackIPv4,
    4041,
  );
  print('Listening on http://${server.address.address}:${server.port}/');
  await for (var request in server) {
    handleRequest(request);
  }
  print('Server launched');
}

void handleRequest(HttpRequest request) {
  try {
    if (request.method == 'GET') {
      handleGet(request);
    } else {
      request.response
        ..statusCode = HttpStatus.methodNotAllowed
        ..write('Unsupported request: ${request.method}.')
        ..close();
    }
  } catch (e) {
    print('Exception in handleRequest: $e');
  }
  print('Request handled.');
}

void handleGet(HttpRequest request) {
  var random = intGenerator.nextInt(100) + 100;
  final width = 480;
  final height = 320;
  final jsonObj = <String, List<String>>{
    'images': [
      'https://picsum.photos/id/${random}/${width}/${height}',
      'https://picsum.photos/id/${random + 1}/${width}/${height}',
      'https://picsum.photos/id/${random + 2}/${width}/${height}',
      'https://picsum.photos/id/${random + 3}/${width}/${height}',
      'https://picsum.photos/id/${random + 4}/${width}/${height}'
    ]
  };
  final response = request.response;

  response
    ..statusCode = HttpStatus.ok
    ..headers.contentType = ContentType.json
    ..headers.add('Access-Control-Allow-Origin', '*')
    ..headers.add('Access-Control-Allow-Methods', 'GET')
    ..writeln(json.encode(jsonObj))
    ..close();
  print('Handel a request');
}
